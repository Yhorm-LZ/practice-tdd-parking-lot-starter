ParkingLotTest
case 1 - Given a parking lot and a car,when park the car,then return a parking ticket

case 2 - Given a parking lot ,two different cars, when park the cars ,return different parking tickets

case 3 - Given a parking lot ,a ticket, when fetch car ,return the right car

case 4 - Given a parking lot with max capacity, a car,when park car ,throw exception

case 5 - Given a parking lot,a ticket,when fetch by the ticket twice,the second fetch throw exception

case 6 - Given a parking lot,a wrong ticket, when fetch by the ticket,throw exception

StandardParkingBoyTest
case 1 - Given a standard parking boy, a car, when park the car,then return a parking ticket

case 2 - Given a standard parking boy, a ticket, when fetch a car,then return the right car

case 3 - Given a standard parking boy ,two different cars, when park the cars ,return different parking tickets

case 4 - Given a standard parking boy with max capacity, a car,when park car ,throw exception

case 5 - Given a standard parking boy,a ticket,when fetch by the ticket twice,the second fetch throw exception

case 6 - Given a standard parking boy,a wrong ticket, when fetch by the ticket,throw exception

case 7 - Given a standard parking boy,two parking lot both not full,when part a car,park in first parking lot,and return ticket

case 8 - Given a standard parking boy, first parking lot is full,second is not full,when park a car,park in second and return ticket

case 9 - Given a standard parking boy who manage two full parking lot,when park a car,throw exception

case 10 - Given a standard parking boy who manage two parking lot,when fetch a car,given wrong ticket,throw exception

case 11 - Given a standard parking boy who manage two parking lot,when fetch a car,given used ticket,throw exception

SmartParkingBoyTest
case 1 - Given a smart parking boy who manage two same available position parking lot,when park a car,park in the first parking lot,and return ticket

case 2 - Given a smart parking boy who manage two parking lot and first available position less than second,when park a car,park in the second parking lot,and return ticket

case 3 - Given a smart parking boy who manage two parking lot,when fetch a car,given valid ticket,return right car

case 4 - Given a smart parking boy who manage two full parking lot,when park a car,throw exception

case 5 - Given a smart parking boy who manage two parking lot,when fetch a car,given wrong ticket,throw exception

case 6 - Given a smart parking boy who manage two parking lot,when fetch a car,given used ticket,throw exception

SuperParkingBoyTest
case 1 - Given a super parking boy who manage two same available position parking lot,when park a car,park in the first parking lot,and return ticket

case 2 - Given a super parking boy who manage two parking lot and first available position rate less than second and first available position more than second,when park a car,park in the second parking lot,and return ticket

case 3 - Given a super parking boy who manage two parking lot,when fetch a car,given valid ticket,return right car

case 4 - Given a super parking boy who manage two full parking lot,when park a car,throw exception

case 5 - Given a super parking boy who manage two parking lot,when fetch a car,given wrong ticket,throw exception

case 6 - Given a super parking boy who manage two parking lot,when fetch a car,given used ticket,throw exception
