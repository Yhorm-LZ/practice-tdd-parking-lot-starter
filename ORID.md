**O(objective)**:通过parking lot的联系熟练了TDD的case的构建，测试方法的命名，given,when,then的写法。熟练了如何把TDD用到OOP项目的开发上去<br><br>**R(Reflective)**:Great
<br><br>**I(Interpretive)**:先构建case在去编写测试用例可以让我们对测试用例的构建过程更加清晰，不容易漏写测试，有助于提交测试的覆盖率。TDD在OOP开发中，每个功能实现类都可以写一个对应的测试类，对该类的功能进行详细、全面的测试。
<br><br>**D(Decisional)**:在接下来OOP开发中，严格按照TDD的流程进行开发，先大致列出test case再写具体的测试代码与功能实现。完成功能后运行测试，保证功能的正确性。