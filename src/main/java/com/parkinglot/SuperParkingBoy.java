package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SuperParkingBoy extends ParkingPerson{
    public SuperParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    public ParkingTicket park(Car car) {
        return parkingLots.stream()
                .filter(ParkingLot::hasAvailablePosition)
                .max(Comparator.comparing(ParkingLot::getAvailablePositionRate))
                .orElseThrow(()->new NoAvailablePositionException("No available position"))
                .park(car);
    }

}
