package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private final Map<ParkingTicket, Car> ticketCarMap = new HashMap<>();
    private final int capacity;

    public ParkingLot() {
        capacity = 10;
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public int getRestAvailablePosition(){
        return capacity-ticketCarMap.size();
    }
    public double getAvailablePositionRate(){return (double) getRestAvailablePosition()/capacity;}

    public Boolean hasAvailablePosition() {
        return ticketCarMap.size() < capacity;
    }

    public Boolean isRecognizedTicket(ParkingTicket parkingTicket) {
        return ticketCarMap.get(parkingTicket) != null;
    }

    public ParkingTicket park(Car car) {
        if (!hasAvailablePosition()) {
            throw new NoAvailablePositionException("No available position");
        }
        ParkingTicket parkingTicket = new ParkingTicket();
        ticketCarMap.put(parkingTicket, car);
        return parkingTicket;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        Car fetchedCar = ticketCarMap.get(parkingTicket);
        if (fetchedCar == null) {
            throw new UnrecognizedParkingTicketException("Unrecognized parking ticket");
        }
        ticketCarMap.remove(parkingTicket);
        return fetchedCar;
    }
}
