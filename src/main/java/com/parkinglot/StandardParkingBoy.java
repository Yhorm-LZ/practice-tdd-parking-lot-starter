package com.parkinglot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class StandardParkingBoy extends ParkingPerson{
    public StandardParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }
}
