package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy extends ParkingPerson {


    public SmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car) {
        return parkingLots.stream()
                .filter(ParkingLot::hasAvailablePosition)
                .max(Comparator.comparing(ParkingLot::getRestAvailablePosition))
                .orElseThrow(()->new NoAvailablePositionException("No available position"))
                .park(car);
    }


}
