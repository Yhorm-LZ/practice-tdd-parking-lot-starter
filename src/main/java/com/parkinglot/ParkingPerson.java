package com.parkinglot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParkingPerson {
    protected final List<ParkingLot> parkingLots =new ArrayList<>();

    public ParkingPerson() {
    }

    public ParkingPerson(ParkingLot... parkingLots) {
        this.parkingLots.addAll(Arrays.asList(parkingLots));
    }

    public void manage(ParkingLot parkingLot){
        this.parkingLots.add(parkingLot);
    }

    public ParkingTicket park(Car car) {
        return parkingLots.stream()
                .filter(ParkingLot::hasAvailablePosition)
                .findFirst()
                .orElseThrow(()->new NoAvailablePositionException("No available position"))
                .park(car);
    }

    public Car fetch(ParkingTicket parkingTicket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isRecognizedTicket(parkingTicket))
                .findAny().orElseThrow(()->new UnrecognizedParkingTicketException("Unrecognized parking ticket"))
                .fetch(parkingTicket);
    }
}
