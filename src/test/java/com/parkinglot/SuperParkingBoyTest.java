package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SuperParkingBoyTest {
    @Test
    void should_park_in_first_parking_lot_when_park_given_a_car_and_two_have_same_available_position_parking_lot(){
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        superParkingBoy.manage(parkingLot1);
        superParkingBoy.manage(parkingLot2);

        ParkingTicket parkingTicket = superParkingBoy.park(car);
        Car fetchedCar = parkingLot1.fetch(parkingTicket);

        Assertions.assertEquals(car, fetchedCar);
    }
    @Test
    void should_return_right_car_when_fetch_given_a_ticket() {
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot);
        ParkingTicket parkingTicket = superParkingBoy.park(car1);

        Car car2 = superParkingBoy.fetch(parkingTicket);
        Assertions.assertEquals(car1, car2);
    }
    @Test
    void should_park_in_second_parking_lot_when_park_given_a_car_and_two_parking_lot_first_available_position_rate_less_than_second_and_first_available_position_more_than_second() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(2);
        parkingLot1.park(new Car());
        parkingLot1.park(new Car());
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        superParkingBoy.manage(parkingLot1);
        superParkingBoy.manage(parkingLot2);

        ParkingTicket parkingTicket = superParkingBoy.park(car);
        Car fetchedCar = parkingLot2.fetch(parkingTicket);

        Assertions.assertEquals(car, fetchedCar);
    }
    @Test
    void should_throw_exception_when_park_given_a_car_and_two_full_parking_lot() {
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        superParkingBoy.manage(parkingLot1);
        superParkingBoy.manage(parkingLot2);

        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> superParkingBoy.park(new Car()));
        Assertions.assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_two_parking_lot_and_wrong_ticket() {
        Car car = new Car();
        ParkingTicket parkingTicket = new ParkingTicket();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        superParkingBoy.manage(parkingLot1);
        superParkingBoy.manage(parkingLot2);
        superParkingBoy.park(car);

        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> superParkingBoy.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_two_parking_lot_and_used_ticket() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy();
        superParkingBoy.manage(parkingLot1);
        superParkingBoy.manage(parkingLot2);
        ParkingTicket parkingTicket = superParkingBoy.park(car);
        superParkingBoy.fetch(parkingTicket);

        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> superParkingBoy.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }
}
