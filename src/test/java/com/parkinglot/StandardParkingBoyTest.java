package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StandardParkingBoyTest {
    @Test
    void should_return_parkingTicket_when_park_given_a_car() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);

        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_throw_exception_when_park_given_a_car_and_max_capacity_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        for (int i = 0; i < 10; i++) {
            standardParkingBoy.park(new Car());
        }

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> standardParkingBoy.park(new Car()));
        Assertions.assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_return_right_car_when_fetch_given_a_ticket() {
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        ParkingTicket parkingTicket = standardParkingBoy.park(car1);

        Car car2 = standardParkingBoy.fetch(parkingTicket);
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_return_two_right_car_when_fetch_given_two_different_ticket() {
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        ParkingTicket parkingTicket1 = standardParkingBoy.park(car1);
        ParkingTicket parkingTicket2 = standardParkingBoy.park(car2);

        Car fetchedCar1 = standardParkingBoy.fetch(parkingTicket1);
        Car fetchedCar2 = standardParkingBoy.fetch(parkingTicket2);

        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
        Assertions.assertNotEquals(fetchedCar1, fetchedCar2);
    }

    @Test
    void should_throw_exception_fetch_given_a_used_ticket() {
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);
        ParkingTicket parkingTicket = standardParkingBoy.park(car1);

        standardParkingBoy.fetch(parkingTicket);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> standardParkingBoy.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_a_wrong_ticket() {
        ParkingTicket parkingTicket = new ParkingTicket();
        ParkingLot parkingLot = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> standardParkingBoy.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_park_in_first_parking_lot_when_park_given_a_car_and_two_not_full_parking_lot() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manage(parkingLot1);
        standardParkingBoy.manage(parkingLot2);

        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        Car fetchedCar = parkingLot1.fetch(parkingTicket);

        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    void should_park_in_second_parking_lot_when_park_given_a_car_and_first_full_parking_lot_and_second_not_full_parking_lot() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manage(parkingLot1);
        standardParkingBoy.manage(parkingLot2);

        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        Car fetchedCar = parkingLot2.fetch(parkingTicket);

        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_right_car_each_when_fetch_twice_given_two_car_and_two_parking_lot() {
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manage(parkingLot1);
        standardParkingBoy.manage(parkingLot2);

        ParkingTicket parkingTicket1 = standardParkingBoy.park(car1);
        ParkingTicket parkingTicket2 = standardParkingBoy.park(car2);
        Car fetchedCar1 = standardParkingBoy.fetch(parkingTicket1);
        Car fetchedCar2 = standardParkingBoy.fetch(parkingTicket2);

        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_throw_exception_when_park_given_a_car_and_two_full_parking_lot() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manage(parkingLot1);
        standardParkingBoy.manage(parkingLot2);

        Exception exception = Assertions.assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(new Car()));
        Assertions.assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_two_parking_lot_and_wrong_ticket() {
        Car car = new Car();
        ParkingTicket parkingTicket = new ParkingTicket();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manage(parkingLot1);
        standardParkingBoy.manage(parkingLot2);
        standardParkingBoy.park(car);

        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_two_parking_lot_and_used_ticket() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.manage(parkingLot1);
        standardParkingBoy.manage(parkingLot2);
        ParkingTicket parkingTicket = standardParkingBoy.park(car);
        standardParkingBoy.fetch(parkingTicket);

        Exception exception = Assertions.assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

}
