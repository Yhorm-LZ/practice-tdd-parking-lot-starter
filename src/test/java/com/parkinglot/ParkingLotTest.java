package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_parkingTicket_when_park_given_a_car() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();

        ParkingTicket parkingTicket = parkingLot.park(car);

        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_right_car_when_fetch_given_a_ticket() {
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(car1);

        Car car2 = parkingLot.fetch(parkingTicket);
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_return_two_right_car_when_fetch_given_two_different_ticket() {
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

        Car fetchedCar1 = parkingLot.fetch(parkingTicket1);
        Car fetchedCar2 = parkingLot.fetch(parkingTicket2);

        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
        Assertions.assertNotEquals(fetchedCar1, fetchedCar2);
    }

    @Test
    void should_throw_exception_fetch_given_a_used_ticket() {
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket parkingTicket = parkingLot.park(car1);

        parkingLot.fetch(parkingTicket);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingLot.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_exception_when_park_given_a_car_and_max_capacity_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(10);
        for (int i = 0; i < 10; i++) {
            parkingLot.park(new Car());
        }

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingLot.park(new Car()));
        Assertions.assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_a_wrong_ticket() {
        ParkingTicket parkingTicket = new ParkingTicket();
        ParkingLot parkingLot = new ParkingLot();

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingLot.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }
}
